package com.example.sampledatabase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private UsuarioSQLiteHelper usdbh;
    private SQLiteDatabase db;
    private Button btnInsert, btnUpdate, btnDelete, btnSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnInsert = findViewById (R.id.btnInsertar);
        btnUpdate = findViewById (R.id.btnActualizar);
        btnDelete = findViewById (R.id.btnEliminar);
        btnSelect = findViewById (R.id.btnConsultar);


        usdbh = new UsuarioSQLiteHelper(this, "DBUsuarios", null, 1);
        db = usdbh.getWritableDatabase();

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(MainActivity.this, "Has pulsado Insertar", Toast.LENGTH_LONG).show();
                ContentValues content = new ContentValues();
                content.put("codigo", ((EditText)findViewById(R.id.txtReg)).getText().toString());
                content.put("nombre", ((EditText)findViewById(R.id.txtVal)).getText().toString());
                db.insert("Usuarios", null, content);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Has pulsado Actualizar", Toast.LENGTH_LONG).show();
                ContentValues content = new ContentValues();
                content.put("nombre", ((EditText)findViewById(R.id.txtVal)).getText().toString());
                String valor = ((EditText)findViewById(R.id.txtReg)).getText().toString();
                String[] array = {valor};
                db.update("Usuarios", content, "codigo = ?", array );

            }
        });

        btnDelete.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Has pulsado Borrar", Toast.LENGTH_LONG).show();
                String valor = ((EditText)findViewById(R.id.txtReg)).getText().toString();
                String[] array = {valor};
                db.delete("Usuarios", "codigo = ?", array );
            }
        });

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor c = db.rawQuery ("SELECT * FROM USUARIOS",null);
                String texto, codigo, nombre;
                texto = "";



                if (c.moveToFirst())
                {
                    do {
                        codigo = c.getString(0);
                        nombre = c.getString(1);
                        texto += "codigo: " + codigo + " nombre: " + nombre + "\n";

                    }
                    while (c.moveToNext());
                }
                TextView tvResultado = findViewById (R.id.txtResultado);
                tvResultado.setText(texto);

            }
        });


    }
}
