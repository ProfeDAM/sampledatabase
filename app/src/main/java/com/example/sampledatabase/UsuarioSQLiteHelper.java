package com.example.sampledatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class UsuarioSQLiteHelper extends SQLiteOpenHelper {

    public final String sqlCreate = "CREATE TABLE Usuarios (codigo INTEGER, nombre TEXT)";

    public UsuarioSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sentencia;
        db.execSQL(sqlCreate);

        for (int i=1; i<=5; i++)
        {
            sentencia = "INSERT INTO USUARIOS VALUES ( " + i + ", 'Usuario" + i +"')";
            db.execSQL (sentencia);
        }



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion == 1 && newVersion ==2)
        {
            db.execSQL ("CREATE TABLE PERFIL (ID_PERFIL INTEGER, ID_USUARIO INTEGER, NOMBRE TEXT)");
        }

        else if (oldVersion ==2 && newVersion ==1)
        {
            db.execSQL ("DROP TABLE IF EXISTS PERFIL");

        }

    }
}
